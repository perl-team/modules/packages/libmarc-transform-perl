Source: libmarc-transform-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Robin Sheat <robin@catalyst.net.nz>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libmarc-record-perl <!nocheck>,
                     libyaml-perl <!nocheck>
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmarc-transform-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmarc-transform-perl.git
Homepage: https://metacpan.org/release/MARC-Transform
Rules-Requires-Root: no

Package: libmarc-transform-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libmarc-record-perl,
         libyaml-perl
Description: Perl module to transform a MARC record with a yaml configuration file
 MARC::Transform transforms a MARC record using a YAML configuration file.
 .
 It allows you to create, update, delete, and duplicate fields and subfields of
 a record. You can also use scripts and lookup tables. You can specify
 conditions to execute these actions.
 .
 All conditions, actions, functions and lookup tables are defined by YAML.
